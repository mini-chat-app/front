FROM node:alpine as build
WORKDIR /front

ADD package.json ./
ADD tsconfig.json ./
RUN yarn install --silent

ADD src ./src
ADD public ./public
RUN yarn run build

FROM nginx:alpine
COPY --from=build /front/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]