import React, { ChangeEvent } from "react";
import { Store } from '../stores';
import { observer } from 'mobx-react';
import { EntryTable } from "../components/table"
import client, { addEntryMutation, getEntriesQuery, resetCache } from "../client"
import { action } from 'mobx';

export const Home = observer((store: Store) => {
    let currentEntry = ""
    let searchInputValue = ""

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target
        switch (event.target.id) {
            case "searchInput":
                searchInputValue = value
                break;

            case "addInput":
                currentEntry = value
                break;

        }
    }

    const addEntry = action(() => {
        client.mutate(addEntryMutation(store.UserStore.User.username, currentEntry))
            .then(action((result: any) => {
                let { id, username, sentence, createdAt } = result.data.addEntry
                if (!~store.EntryStore.entries.findIndex((e) => e.id === id)) {
                    store.EntryStore.entries.push({ id, username, sentence, createdAt })
                }
                currentEntry = ""
                let input = document.getElementById('addInput') as HTMLInputElement
                input!.value = ""
            }))
    })

    const AddEntryComponent = observer(() => {
        return (
            store.UserStore.User.username === "" ?
                <div></div> :
                <div style={{ textAlign: "center" }}>
                    <input id="addInput" onChange={handleChange} type="text" />
                    <button style={{ marginTop: 16, marginBottom: 16, textAlign: "center" }} onClick={addEntry}>Ajout</button>
                </div>
        )
    })

    const filterByUser = action((actionType: string = '') => {
        if (actionType === 'reset') {
            let input = document.getElementById('searchInput') as HTMLInputElement
            input!.value = ""
            searchInputValue = ''
        }

        resetCache()
        client.query(getEntriesQuery(searchInputValue))
            .then(action((result: any) => {
                store.EntryStore.entries = result.data.entries
            }))
    })

    const FilterByUserComponent = observer(() => {
        return (
            <div style={{ textAlign: "center", marginTop: 16, marginBottom: 16 }}>
                <input id="searchInput" onChange={handleChange} type="text" />
                <button onClick={() => filterByUser()}>Filtrer</button>
                <button onClick={() => filterByUser('reset')}>Reset</button>
            </div>
        )
    })

    return (
        <div>
            <FilterByUserComponent></FilterByUserComponent>
            <EntryTable {...store}></EntryTable>
            <AddEntryComponent></AddEntryComponent>
        </div>
    )
})