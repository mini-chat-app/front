export interface Entry{
    id: number,
    username: string,
    sentence: string,
    createdAt: number
}