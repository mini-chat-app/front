import React from 'react';
import './App.css';
import { Home } from './pages/home';
import { Header } from './components/header';
import { Store } from './stores';

const store = new Store()

function App() {
  return (
    <div className="App">
      <Header {...store.UserStore}></Header>
      <Home {...store}></Home>
    </div>
  );
}

export default App;