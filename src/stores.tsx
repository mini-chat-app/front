import { EntryStore } from "./stores/EntryStore";
import { UserStore } from "./stores/UserStore";

export class Store{
    EntryStore: EntryStore
    UserStore: UserStore

    constructor(){
        this.EntryStore = new EntryStore()
        this.UserStore = new UserStore()
    }
}