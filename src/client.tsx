import { ApolloClient, InMemoryCache, HttpLink, gql } from '@apollo/client';
import { API_ENDPOINT } from './contants'

const link = new HttpLink({ uri: API_ENDPOINT + '/graphql' })
const cache = new InMemoryCache()
const client = new ApolloClient({ link, cache })

export const deleteEntryMutation = (id: number) => {
    return {
        mutation: gql`
        mutation {
            deleteEntry(id: ${id}){
                id
            }
        }
    `
    }
}

export const addEntryMutation = (username: string, sentence: string) => {
    return {
        mutation: gql`
            mutation {
                addEntry(username: "${username}", sentence:"${sentence}"){
                    id
                    username
                    sentence
                    createdAt
                }
            }
        `
    }
}

export const getEntriesQuery = (username: string = '') => {
    return {
        query: gql`
            query {
                entries(username: "${username}") {
                    id
                    username
                    sentence,
                    createdAt
                }
            }
        `
    }
}

export const resetCache = () => {
    cache.reset()
}

export default client