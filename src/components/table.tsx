import React from "react";
import { observer } from 'mobx-react';
import { Entry } from '../models/Entry';
import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody } from "@material-ui/core";
import { Store } from '../stores';
import client, { deleteEntryMutation } from "../client"
import { action } from 'mobx';
import moment from "moment";

export const EntryTable = observer((store: Store) => {
    const { entries } = store.EntryStore
    const { username } = store.UserStore.User

    const deleteEntry = (id: number) => {
        client.mutate(deleteEntryMutation(id))
            .then(action((result: any) => {
                const deletedId = result.data.deleteEntry!.id
                if (deletedId) {
                    store.EntryStore.entries = entries.filter((e) => e.id !== id)
                }
            }))
    }

    return (
        <TableContainer>
            <Table aria-label="simple table">
                <TableHead style={{ backgroundColor: "gray" }}>
                    <TableRow>
                        <TableCell>Pseudo</TableCell>
                        <TableCell>Phrase</TableCell>
                        <TableCell>Date</TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {entries.map((entry: Entry) => (
                        <TableRow style={{ backgroundColor: username === entry.username ? '#ebebeb' : '' }} key={entry.id}>
                            <TableCell>
                                {entry.username}
                            </TableCell>
                            <TableCell>
                                {entry.sentence}
                            </TableCell>
                            <TableCell>
                                {moment.unix(entry.createdAt / 1000).format("MM/DD/YYYY HH:mm")}
                            </TableCell>
                            <TableCell>
                                {username === entry.username ?
                                    <button onClick={() => deleteEntry(entry.id)}>Supprimer</button> :
                                    <div></div>}
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
})