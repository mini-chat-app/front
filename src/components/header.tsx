import React, { ChangeEvent } from 'react';
import { AppBar, Toolbar } from '@material-ui/core';
import { observer } from 'mobx-react';
import { UserStore } from '../stores/UserStore';
import { action } from 'mobx';

export const Header = observer((UserStore: UserStore) => {
    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        username = event.target.value
    }

    const setUsername = action(() => {
        UserStore.User.username = username
    })

    let username = ''

    return (
        <AppBar position="static">
            <Toolbar variant="dense">
                {UserStore.User.username === '' ?
                    <div>Pseudo : <input onChange={handleChange}></input><button onClick={setUsername}>Ok</button></div> :
                    <div>{UserStore.User.username}</div>
                }
            </Toolbar>
        </AppBar>
    )
})