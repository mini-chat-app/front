import { makeAutoObservable } from 'mobx';
import { User } from '../models/User'

export class UserStore {

    User: User

    constructor() {
        this.User = {
            username: ""
        }

        makeAutoObservable(this)
    }
}