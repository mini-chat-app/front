import { makeAutoObservable } from 'mobx';
import { Entry } from '../models/Entry';
import client, { getEntriesQuery } from "../client"
import { action } from 'mobx';

export class EntryStore {

    entries: Entry[] = []

    constructor() {
        client.query(getEntriesQuery())
            .then(action((result: any) => this.entries = result.data.entries))
            .catch(e => console.log(e))
        makeAutoObservable(this)
    }
}